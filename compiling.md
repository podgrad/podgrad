# PODGRAD
## _Installation_
 

The app is built in Flutter so we need to first install flutter to run this app.Please follow bellow instructions:
- Download the following installation bundle to get the latest stable release of the Flutter SDK. 
    For intel MAC : https://storage.googleapis.com/flutter_infra_release/releases/stable/macos/flutter_macos_3.0.1-stable.zip
    For M1 Mac : https://storage.googleapis.com/flutter_infra_release/releases/stable/macos/flutter_macos_arm64_3.0.1-stable.zip
- Extract the file in the desired location, for example:
 ```sh
cd ~/development
unzip ~/Downloads/flutter_macos_3.0.1-stable.zip
```
- Add the flutter tool to your path:
 ```sh
export PATH="$PATH:`pwd`/flutter/bin"
```
- Run the following command to see if there are any dependencies you need to install to complete the setup (for verbose output, add the -v flag):
```
flutter doctor
```
This command checks your environment and displays a report to the terminal window. The Dart SDK is bundled with Flutter; it is not necessary to install Dart separately. Check the output carefully for other software you might need to install or further tasks to perform (shown in bold text).

For example:

```
[-] Android toolchain - develop for Android devices
    • Android SDK at /Users/obiwan/Library/Android/sdk
    ✗ Android SDK is missing command line tools; download from https://goo.gl/XxQghQ
    • Try re-installing or updating your Android SDK,
      visit https://docs.flutter.dev/setup/#android-setup for detailed instructions.
```

- Platform setup

#### IOS Setup
macOS supports developing Flutter apps for iOS, Android, macOS itself and the web. Complete at least one of the platform setup steps now, to be able to build and run your first Flutter app.

- Install XCode
To develop Flutter apps for iOS, you need a Mac with Xcode installed.

Install the latest stable version of Xcode (using web download or the Mac App Store).
Configure the Xcode command-line tools to use the newly-installed version of Xcode by running the following from the command line:

```
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
sudo xcodebuild -runFirstLaunch
```
his is the correct path for most cases, when you want to use the latest version of Xcode. If you need to use a different version, specify that path instead.

Make sure the Xcode license agreement is signed by either opening Xcode once and confirming or running sudo xcodebuild -license from the command line.
Versions older than the latest stable version may still work, but are not recommended for Flutter development. Using old versions of Xcode to target bitcode is not supported, and is likely not to work.

With Xcode, you’ll be able to run Flutter apps on an iOS device or on the simulator.
#### Android setup
- Install Android Studio
Download and install Android Studio.
Start Android Studio, and go through the ‘Android Studio Setup Wizard’. This installs the latest Android SDK, Android SDK Command-line Tools, and Android SDK Build-Tools, which are required by Flutter when developing for Android.
Run flutter doctor to confirm that Flutter has located your installation of Android Studio. If Flutter cannot locate it, run flutter config --android-studio-dir <directory> to set the directory that Android Studio is installed to
- Setup Android Device
To prepare to run and test your Flutter app on an Android device, you need an Android device running Android 4.1 (API level 16) or higher.

Enable Developer options and USB debugging on your device. Detailed instructions are available in the Android documentation.
Windows-only: Install the Google USB Driver.
Using a USB cable, plug your phone into your computer. If prompted on your device, authorize your computer to access your device.
In the terminal, run the flutter devices command to verify that Flutter recognizes your connected Android device. By default, Flutter uses the version of the Android SDK where your adb tool is based. If you want Flutter to use a different installation of the Android SDK, you must set the ANDROID_SDK_ROOT environment variable to that installation directory.


- Final
If all good. Run 

```
flutter doctor
```

And you will get something positive like bellow

```
tashfik@Tasfiquls-MacBook-Pro podgrad_app % flutter doctor
Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, 2.10.3, on macOS 11.2.3 20D91 darwin-x64, locale
    en-GB)
[✓] Android toolchain - develop for Android devices (Android SDK version 29.0.2)
[!] Xcode - develop for iOS and macOS (Xcode 12.5)
    ! Flutter recommends a minimum Xcode version of 13.
      Download the latest version or update via the Mac App Store.
[✓] Chrome - develop for the web
[✓] Android Studio (version 4.0)
[✓] Connected device (1 available)
[✓] HTTP Host Availability

! Doctor found issues in 1 category.
tashfik@Tasfiquls-MacBook-Pro podgrad_app % 

```


Go ro to project .And run

```
flutter run
```
or if you are using android studio or XCode try their built in button.
 
 
 
 
  