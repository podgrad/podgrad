import 'package:audioplayers/audioplayers.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
//  User user;
//
//  HomePage(this.user);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isPlaying = false;
  bool isPause = false;
  AudioCache audioCache = AudioCache();
  AudioPlayer advancedPlayer = AudioPlayer(playerId: 'podgrad');
  double _value = 0.0;
  String totalTime = '';
  String currentTime = '';
  Duration? totalDuration = null;

  void handlePressedWithParam() {
    advancedPlayer = AudioPlayer(playerId: 'podgrad');
    advancedPlayer.stop();
    setState(() {
//      isPlaying = f;
      isPause = false;
      totalDuration = null;
    });
    advancedPlayer
        .play('https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3');
    advancedPlayer.onDurationChanged.listen((event) {
      setState(() {
        totalTime =
            event.inMinutes.toString() + ":" + event.inSeconds.toString();
        totalDuration = event;
      });
    });

    advancedPlayer.onAudioPositionChanged.listen((Duration p) {
      setState(() {
        currentTime = p.inMinutes.toString() + ":" + p.inSeconds.toString();

        _value = totalDuration == null
            ? 0
            : (p.inSeconds / totalDuration!.inSeconds);
      });
    });
  }

  @override
  void initState() {
    super.initState();
    handlePressedWithParam();
  }

  Widget songPlaying() {
    return WillPopScope(
        child: Scaffold(
          body: Container(
            width: double.infinity,
            height: double.infinity,
            decoration:
                BoxDecoration(shape: BoxShape.rectangle, color: Colors.black),
            child: Container(
              child: Column(
                children: <Widget>[
                  AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Song Name',
                      style: TextStyle(fontSize: 16),
                    ),
                    centerTitle: true,
                    leading: IconButton(
                      icon: Icon(Icons.keyboard_arrow_down),
                      onPressed: () {
                        setState(() {
                          isPlaying = false;
                        });
                      },
                    ),
                    actions: <Widget>[
                      IconButton(
                        icon: Icon(Icons.more_horiz),
                        onPressed: () {},
                      )
                    ],
                  ),
                  Expanded(
                    child: Container(
                      child: CachedNetworkImage(
                        imageUrl:
                            'https://i.pinimg.com/originals/aa/01/49/aa01493b0804a1a69015f0761fee7796.png',
                      ),
                    ),
                  ),
                  //https://www.soundhelix.com/examples/mp3/SoundHelix-Song-16.mp3
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            left: 20,
                          ),
                          child: Text(
                            'Song Name',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            left: 20,
                          ),
                          child: Text(
                            'Singer Name',
                            style:
                                TextStyle(color: Colors.white70, fontSize: 16),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Slider(
                      value: _value,
                      onChanged: (value) {
                        //print(value);
                        setState(() {
                          _value = value;
                        });
//                    print(Duration(milliseconds: (_value*totalDuration.inMilliseconds).toInt()));
//                    advancedPlayer.seek(Duration(microseconds: 200));
                      },
                      activeColor: Colors.white,
                      inactiveColor: Colors.white,

                      // valueColor: AlwaysStoppedAnimation(Color(0xff828A8A)),
                      // backgroundColor: Color(0xff666666),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 25, right: 25),
                    child: Row(
                      children: [
                        Text(
                          currentTime,
                          style: TextStyle(color: Colors.white70, fontSize: 10),
                        ),
                        Spacer(),
                        Text(
                          totalTime,
                          style: TextStyle(color: Colors.white70, fontSize: 10),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        IconButton(
                          iconSize: 25,
                          icon:
                              Icon(Icons.favorite_border, color: Colors.white),
                          onPressed: () {},
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: Icon(Icons.skip_previous, color: Colors.white),
                          onPressed: () {},
                        ),
                        IconButton(
                          iconSize: 50,
                          icon: Icon(
                              isPause
                                  ? Icons.play_circle_filled
                                  : Icons.pause_circle_filled,
                              color: Colors.white),
                          onPressed: () {
                            setState(() {
                              if (isPause) {
                                isPause = false;
                                advancedPlayer.resume();
                              } else {
                                isPause = true;
                                advancedPlayer.pause();
                              }
                            });
                          },
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: Icon(Icons.skip_next, color: Colors.white),
                          onPressed: () {},
                        ),
                        IconButton(
                          iconSize: 30,
                          icon: Icon(Icons.block, color: Colors.white),
                          onPressed: () {
//                        getRandomSong(song.id).then((value) {
//                          setState(() {
//                            lastSong = song;
//                          });
//                          handlePressedWithParam(value);
//                        });
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        onWillPop: _willPopCallback);
  }

  Future<bool> _willPopCallback() async {
    setState(() {
      isPlaying = false;
    });
    return false; // return true if the route to be popped
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Stack(
          children: <Widget>[
            Scaffold(
              body: Container(
                color: Colors.black.withOpacity(0.4),
                child: Stack(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        SizedBox(
                          height: 60,
                          width: MediaQuery.of(context).size.width,
                        )
                      ],
                    ),
                    Positioned(
                      bottom: 0,
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 60,
                        child: Container(
                          color: Colors.black87,
                          child: ListTile(
                            onTap: () {
                              setState(() {
                                isPlaying = true;
                              });
                            },
                            trailing: IconButton(
                              iconSize: 30,
                              icon: Icon(
                                isPause ? Icons.play_arrow : Icons.pause,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                setState(() {
                                  if (isPause) {
                                    isPause = false;
                                    advancedPlayer.resume();
                                  } else {
                                    isPause = true;
                                    advancedPlayer.pause();
                                  }
                                });
                              },
                            ),
                            title: Text(
                              'Song Name',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
//        songPlaying()
//      Container(width: MediaQuery.of(context).size.width,height: 1,color: Colors.white,),
            isPlaying
                ? Container(
                    child: songPlaying(),
                  )
                : Container()
          ],
        ),
        onWillPop: _willPopCallback);
  }
}
