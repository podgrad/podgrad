import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';

import '../utils/db_ops.dart';
import 'login_screen.dart';

class RegistrationPage extends StatefulWidget {
  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  bool isLoading = false;
  TextEditingController passCon = new TextEditingController();
  TextEditingController conPassCon = new TextEditingController();
  TextEditingController userIdCon = new TextEditingController();
  bool isHide = true;
  String? userNameError;
  String? passwordError;
  String? conPasswordError;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 130,
        child: Column(
          children: [
            isLoading
                ? CircularProgressIndicator(
                    color: Colors.black87,
                  )
                : MaterialButton(
                    elevation: 0,
                    color: Colors.black87,
                    height: 50,
                    minWidth: 200,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12)),
                    onPressed: () {
//                      deleteUser();
                      setState(() {
                        userNameError = null;
                        passwordError = null;
                        conPasswordError = null;
                      });
                      String pass = passCon.text.toString();
                      String userId = userIdCon.text.toString();
                      if (userId.length < 1) {
                        setState(() {
                          userNameError = "Enter Username !";
                        });
                      } else if (pass.length < 1) {
                        setState(() {
                          passwordError = "Enter Password !";
                        });
                      } else if (conPassCon.text.length < 1) {
                        setState(() {
                          conPasswordError = "Enter Confirm Password !";
                        });
                      } else if (!conPassCon.text.contains(pass)) {
                        setState(() {
                          conPasswordError = "Passwords Doesn't match!";
                        });
                      } else {
//                        setState(() {
//                          isLoading = true;
//                        });
                        saveUser(userId, pass).then((value) {
                          if (value == null) {
                            setState(() {
                              userNameError = "Username already exists !";
                            });
                          } else {
                            setState(() {
                              userIdCon.text = "";
                              passCon.text = "";
                              conPassCon.text = "";
                            });
                            Fluttertoast.showToast(
                                msg: 'Successful !',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          }
                        });
                      }
                    },
                    child: Text(
                      "SUBMIT",
                      style: GoogleFonts.poppins(
                        fontSize: 16,
                        color: Color(0xffFFFFFF),
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
            GestureDetector(
              child: Text(
                "\nAlready have an account ? LOGIN ? \n",
                style: GoogleFonts.poppins(
                  fontSize: 15,
                  height: 1.6,
                  color: Colors.black87,
                  fontWeight: FontWeight.w500,
                ),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Text(
          "Registration".toUpperCase(),
          style: GoogleFonts.poppins(
            fontSize: 16,
            color: Color(0xffFFFFFF),
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Provide Bellow Information For Registration",
              style: GoogleFonts.poppins(
                fontSize: 15,
                height: 1.6,
                color: Colors.black87,
                fontWeight: FontWeight.w500,
              ),
            ),
            Container(
              height: 10,
            ),
            Container(
              margin: EdgeInsets.only(top: 12),
              width: MediaQuery.of(context).size.width - 55,
              child: new TextField(
//                enabled: widget.type == 1 ? true : false,
                controller: userIdCon,
                decoration: new InputDecoration(
                  prefixIcon: Icon(
                    Icons.person,
                    color: Colors.black87,
                  ),
                  hintStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  errorText: userNameError,
                  labelText: "Username",
                  hintText: "Username",
                  labelStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: const BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(
                      color: Colors.black87,
                    ),
                  ),
                ),
              ),
              height: 50,
            ),
            Container(
              margin: EdgeInsets.only(top: 12),
              width: MediaQuery.of(context).size.width - 55,
              child: new TextField(
                obscureText: isHide,
                controller: passCon,
                decoration: new InputDecoration(
                  suffixIcon: GestureDetector(
                    child: Icon(
                      Icons.remove_red_eye,
                      color: Colors.black87,
                    ),
                    onTap: () {
                      setState(() {
                        isHide = !isHide;
                      });
                    },
                  ),
                  errorText: passwordError,
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.black87,
                  ),
                  hintStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  labelText: "Password",
                  hintText: '',
                  labelStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: const BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(
                      color: Colors.black87,
                    ),
                  ),
                ),
              ),
              height: 50,
            ),
            Container(
              margin: EdgeInsets.only(top: 12),
              width: MediaQuery.of(context).size.width - 55,
              child: new TextField(
                obscureText: isHide,
                controller: conPassCon,
                decoration: new InputDecoration(
                  suffixIcon: GestureDetector(
                    child: Icon(
                      Icons.remove_red_eye,
                      color: Colors.black87,
                    ),
                    onTap: () {
                      setState(() {
                        isHide = !isHide;
                      });
                    },
                  ),
                  errorText: conPasswordError,
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.black87,
                  ),
                  hintStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  labelText: "Confirm Password",
                  hintText: '',
                  labelStyle: GoogleFonts.poppins(
                    fontSize: 15,
                    color: Color(0xff1B1D28),
                    fontWeight: FontWeight.w500,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    borderSide: const BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    borderSide: BorderSide(
                      color: Colors.black87,
                    ),
                  ),
                ),
              ),
              height: 50,
            ),
          ],
        ),
      ),
    );
  }
}
