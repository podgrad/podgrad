import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

Future<dynamic> getAllUser() async {
  final prefs = await SharedPreferences.getInstance();
  final value = prefs.getString('user');

  if (value == null || value.length < 1) {
    return [];
  } else {
    return json.decode(value);
  }
}

saveUser(String username, String password) async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'user';
  var v = await getUserByUserName(username);
  print(v);
  if (v != null) {
    return null;
  } else {
    dynamic users = await getAllUser();
    users.add({"username": username, "password": password});
    prefs.setString(key, json.encode(users));
    return username;
  }

  return null;
}

getUserByUserName(username) async {
  dynamic users = await getAllUser();
  for (dynamic user in users) {
    if (user['username'].contains(username)) {
      return user;
    }
  }
  return null;
}

loginDB(username, password) async {
  dynamic users = await getAllUser();
  for (dynamic user in users) {
    if (user['username'].contains(username) &&
        user['password'].contains(password)) {
      return user;
    }
  }
  return null;
}

deleteUser() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'user';
  prefs.clear();
  return null;
}
